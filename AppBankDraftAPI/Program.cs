﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;

namespace AppBankDraftAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                    .UseStartup<Startup>();

        //public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        //{
        //    var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

        //    var webHost = env == "Development" ? 
        //                    WebHost.CreateDefaultBuilder(args)
        //                    .UseStartup<Startup>()
        //                : WebHost.CreateDefaultBuilder(args)
        //                    .UseUrls("http://192.168.1.96:5000")
        //                    .UseKestrel()
        //                    .UseStartup<Startup>();

        //    return webHost;
        //}

    }
}
