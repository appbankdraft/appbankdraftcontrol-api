﻿using AppBankDraftAPI.DAL;
using AppBankDraftAPI.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AppBankDraftAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class LoginsController : ControllerBase
    {
        private readonly AppBankDraft_DbContext _context;
        private readonly IConfiguration _config;
        //private readonly IUtils _utils;

        public LoginsController(AppBankDraft_DbContext context, IConfiguration config/*, IUtils utils*/)
        {
            _context = context;
            _config = config;
            //_utils = utils;
        }

        // GET: api/Logins
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Login>>> GetLogin()
        {
            return await _context.Login.ToListAsync();
        }

        // GET: api/Logins/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Login>> GetLogin(int id)
        {
            var login = await _context.Login.FindAsync(id);

            if (login == null)
            {
                return NotFound();
            }

            return login;
        }

        // POST: api/Logins
        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<Login>> PostLogin(Login login)
        {
            try
            {
                var userFromRepo = await _context.Login.FirstOrDefaultAsync(usr => usr.Username.ToLower() == login.Username.ToLower() && usr.Password == login.Password);

                if (userFromRepo == null)
                    return Unauthorized();

                var claims = new[]
                {
                    new Claim(ClaimTypes.NameIdentifier, userFromRepo.Id.ToString()),
                    new Claim(ClaimTypes.Name, userFromRepo.Username)
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("AppSettings:Token").Value));

                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(claims),
                    Expires = DateTime.Now.AddDays(1),
                    SigningCredentials = creds
                };

                var tokenHandler = new JwtSecurityTokenHandler();
                var token = tokenHandler.CreateToken(tokenDescriptor);

                Log log = new Log()
                {
                    LogMessage = $"O utilizador {login.Username} efetou login",
                    CreatedDate = DateTime.Now
                };

                _context.Log.Add(log);
                await _context.SaveChangesAsync();

                return Ok(new { token = tokenHandler.WriteToken(token), email = userFromRepo.Username });
            }
            catch (Exception e)
            {
                //_utils.LogRegister($"O utilizador {login.Username} tentou efetuar login. [ERROR] {e.Message} [ERROR]");
                throw (e);
            }

        }

        private bool LoginExists(int id)
        {
            return _context.Login.Any(e => e.Id == id);
        }
    }
}
