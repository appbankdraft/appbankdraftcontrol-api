﻿using AppBankDraftAPI.DAL;
using AppBankDraftAPI.Model;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace AppBankDraftAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DraftsController : ControllerBase
    {
        private readonly AppBankDraft_DbContext _context;
        private readonly IConfiguration _config;

        public DraftsController(AppBankDraft_DbContext context, IConfiguration config)
        {
            _context = context;
            _config  = config;
        }

        #region GET: api/Drafts - Get all drafts ordered by paymente date 
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Draft>>> GetDrafts()
        {
            IEnumerable<Draft> data = new List<Draft>();

            try
            {
                data = await _context.Drafts.OrderBy( s => s.PaymentDate).ToListAsync();
                return Ok(data);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region GET: api/Drafts - Get all drafts that are for payment
        [HttpGet("GetInProgressDrafts")]
        public async Task<ActionResult<IEnumerable<Draft>>> GetInProgressDrafts()
        {
            IEnumerable<Draft> data = new List<Draft>();
            DateTime tmpDate = DateTime.Now;
            try
            {
                data = await _context.Drafts.Where(d => !d.IsPaid && !d.IsCanceled && !d.IsDeleted).OrderBy( date => date.PaymentDate ).ToListAsync();
                return Ok(data);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region GET: api/Drafts - Get all drafts that were paid or canceled
        [HttpGet("GetPaidAndCanceled")]
        public async Task<ActionResult<IEnumerable<Draft>>> GetPaidAndCanceled()
        {
            IEnumerable<Draft> data = new List<Draft>();
            DateTime tmpDate = DateTime.Now;
            try
            {
                data = await _context.Drafts.Where(d => !d.IsDeleted).Where(d => d.IsPaid || d.IsCanceled).OrderBy(date => date.PaymentDate).ToListAsync();
                return Ok(data);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        #endregion

        #region GET: api/Drafts/5 - Get one draft by ID
        [HttpGet("{id}")]
        public async Task<ActionResult<Draft>> GetDraft(int id)
        {
            var draft = await _context.Drafts.FindAsync(id);

            if (draft == null || draft.IsDeleted)
            {
                return NotFound();
            }

            return draft;
        }
        #endregion

        #region PUT: api/Drafts/5 Normal Update
        [HttpPut("{id}")]
        public async Task<IActionResult> PutDraft(int id, Draft draft)
        {
            if (id != draft.Id)
            {
                return BadRequest();
            }

            _context.Entry(draft).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DraftExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }
        #endregion

        #region POST: api/Drafts
        [HttpPost]
        public async Task<ActionResult<Draft>> PostDraft()
        {
            string pathFromConfig = _config.GetSection("AppSettings:PathToImages").Value;
            var form = HttpContext.Request.Form;
            dynamic data = JsonConvert.DeserializeObject(form["data"]);
            IFormFile foto = null;

            if (form == null)
            {
                return BadRequest();
            }

            if (form.Files.Count() > 0)
            {
                foto = form.Files[0];
                //var currentDir = Directory.GetCurrentDirectory();
                //var tmpPath = Path.GetDirectoryName(currentDir);
                //tmpPath = $"{tmpPath}\\html\\assets\\drafts";

                var filePath = Path.Combine(pathFromConfig, foto.FileName);

                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await foto.CopyToAsync(fileStream);
                }
            }

            Draft draft = new Draft()
            {
                DraftNumber             = data.draftNumber,
                Supplier                = data.supplier,
                PaymentDate             = data.paymentDate,
                Value                   = data.value,
                StoredUpValue           = data.storedUpValue,
                Photo                   = foto.FileName,
                Notes                   = data.notes,
                IsPaid                  = data.isPaid,
                IsCanceled              = data.isCanceled,
                IsOutdatedAndNotPaid    = data.isOutdatedAndNotPaid,
                IsDeleted               = false
            };

            _context.Drafts.Add(draft);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetDraft", new { id = draft.Id }, draft);
        }
        #endregion

        #region DELETE: api/Drafts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Draft>> DeleteDraft(int id)
        {
            var draft = await _context.Drafts.FindAsync(id);
            if (draft == null)
            {
                return NotFound();
            }

            _context.Drafts.Remove(draft);
            await _context.SaveChangesAsync();

            return draft;
        }
        #endregion

        private bool DraftExists(int id)
        {
            return _context.Drafts.Any(e => e.Id == id);
        }
    }
}
