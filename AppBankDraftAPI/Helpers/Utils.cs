﻿using AppBankDraftAPI.DAL;
using AppBankDraftAPI.Helpers.Interfaces;
using AppBankDraftAPI.Model;
using System;
using System.Threading.Tasks;

namespace AppBankDraftAPI.Helpers
{
    public class Utils : IUtils
    {
        private readonly AppBankDraft_DbContext _context;

        public Utils(AppBankDraft_DbContext context)
        {
            _context = context;
        }

        public async void LogRegister(string message)
        {
            try
            {
                Log log = new Log()
                {
                    LogMessage = message,
                    CreatedDate = DateTime.Now
                };

                _context.Log.Add(log);
                await _context.SaveChangesAsync();
            }
            catch (Exception e)
            {
                throw(e);
            }
        }
    }
}
