﻿using AppBankDraftAPI.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppBankDraftAPI.DAL
{
    public class AppBankDraft_DbContext : DbContext
    {
        public AppBankDraft_DbContext(DbContextOptions<AppBankDraft_DbContext> options) : base(options)
        {
        }

        public DbSet<Draft> Drafts { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Log> Log { get; set; }
        public DbSet<Login> Login { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Draft>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Supplier>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Login>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();

            modelBuilder.Entity<Log>()
                .Property(p => p.Id)
                .ValueGeneratedOnAdd();
        }

    }
}
