﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppBankDraftAPI.Model
{
    public class Supplier: BaseEntity
    {
        public string Name { get; set; }
    }
}
