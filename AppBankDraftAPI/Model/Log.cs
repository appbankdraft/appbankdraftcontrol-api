﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppBankDraftAPI.Model
{
    public class Log : BaseEntity
    {
        public string LogMessage { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
