﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppBankDraftAPI.Model
{
    public class Draft : BaseEntity
    {
        public string DraftNumber { get; set; }
        public string Supplier { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal Value { get; set; }
        public decimal StoredUpValue { get; set; }
        public string Photo { get; set; }
        public string Notes { get; set; }
        public bool IsPaid { get; set; }
        public bool IsCanceled { get; set; }
        public bool IsOutdatedAndNotPaid { get; set; }
        public bool IsDeleted { get; set; }

    }
}
