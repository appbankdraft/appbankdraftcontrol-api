﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppBankDraftAPI.Model
{
    public class Login : BaseEntity
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
