﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppBankDraftAPI.Model
{
    public class LogData
    {
        public string Who { get; set; }
        public string Message { get; set; }
        public string Error { get; set; }
    }
}
